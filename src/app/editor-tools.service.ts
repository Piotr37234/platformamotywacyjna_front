import {Injectable} from '@angular/core';
import {DataService} from './data.service';
import {UserLoggedToolsService} from './user-logged-tools.service';
import {
  AnswerPacket,
  CategoryPacket,
  CoursePacket,
  ElementLessonPacket,
  LessonCoursePacket,
  LessonUserPacket,
  QuestionPacket,
  UserPacket
} from './dataPacket';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditorToolsService {
  private editedCourse: CoursePacket;
  private editedLesson: LessonCoursePacket;

  constructor(private dataService: DataService, private user: UserLoggedToolsService) {
    this.loadNewCourse();
    this.loadNewLesson();
  }

  loadNewCourse() {
    this.editedCourse = new class implements CoursePacket {
      autor: UserPacket;
      category: CategoryPacket;
      cost: number;
      description: string;
      id: number;
      img: string;
      lessonCoursePackets: LessonCoursePacket[];
      name: string;
      password: string;
      rank: number;
      rate: number;
    };
    this.editedCourse.category = null;
    this.editedCourse.autor = this.user.getUserLogged();
    this.editedCourse.cost = 0;
    this.editedCourse.description = 'Opis mojego kursu';
    this.editedCourse.img = '';
    this.editedCourse.lessonCoursePackets = [];
    this.editedCourse.name = 'Nazwa mojego kursu';
    this.editedCourse.password = '';
    this.editedCourse.rank = 1;
    this.editedCourse.rate = 0;
  }

  loadNewLesson() {
    this.editedLesson = new class implements LessonCoursePacket {
      cost: number;
      elementLessons: ElementLessonPacket[];
      id: number;
      idNext: number;
      lessonUsers: LessonUserPacket[];
      locked: number;
      name: string;
      type: number;
    };

    this.editedLesson.name = 'Moja lekcja';
    this.editedLesson.type = 1;
    this.editedLesson.elementLessons = [];
    this.editedLesson.idNext = 0;

    const elementLesson: ElementLessonPacket = new class implements ElementLessonPacket {
      content: string;
      header: string;
      id: number;
      name: string;
      questionPackets: QuestionPacket[];
      type: number;
    };
    elementLesson.id = 1;
    elementLesson.name = 'Moj Rozdzial';
    elementLesson.content = '<h2> Moja treść </h2>';
    elementLesson.questionPackets = [];
    this.editedLesson.elementLessons.push(elementLesson);
  }

  // tmp() {
  //   this.dataService.getCourse(1).subscribe(value => {
  //       this.editedCourse = value;
  //       this.editedLesson = value.lessonCoursePackets[0];
  //       console.log(this.editedLesson);
  //     }
  //   );
  // }

  getEditedLesson(): LessonCoursePacket {
    return this.editedLesson;
  }

  setEditedLesson(value: LessonCoursePacket) {
    this.editedLesson = value;
  }

  setCategory(categoryPacket: CategoryPacket) {
    this.getEditedCourse().category = categoryPacket;
  }


  getEditedCourse(): CoursePacket {
    return this.editedCourse;
  }

  getFile(url: string): Observable<File> {
    return this.dataService.getFile(url);
  }

  setEditedCourse(value: CoursePacket) {
    this.editedCourse = value;
  }

  addNewElementLessonToEditedLesson(element: ElementLessonPacket) {
    this.editedLesson.elementLessons.push(element);
  }

  getArrayStandardLessonEditedCourse(): LessonCoursePacket[] {
    return this.editedCourse.lessonCoursePackets.filter(value => value.type === 1);
  }

  getArrayExtraLessonEditedCourse(): LessonCoursePacket[] {
    return this.editedCourse.lessonCoursePackets.filter(value => value.type === 2);
  }

  getArrayTestLessonEditedCourse(): LessonCoursePacket[] {
    return this.editedCourse.lessonCoursePackets.filter(value => value.type === 3);
  }

  getArrayElementEditedCourseLesson(): ElementLessonPacket[] {
    // console.log(this.editedLesson.elementLessons);
    return this.editedLesson.elementLessons;
  }

  getElementEditedCourseLesson(id: number): ElementLessonPacket {
    return this.editedLesson.elementLessons[id];
  }

  updateSelectedCourse() {
    console.log(this.getEditedCourse());
    return this.dataService.updateCourse(this.getEditedCourse());
  }

  deleteCourse(id: number) {
    return this.dataService.deleteCourse(id);
  }

  createSelectedCourse() {
    console.log(this.getEditedCourse());
    return this.dataService.createCourse(this.getEditedCourse());
  }


  addNewQuestion(idElementLesson: number, question: QuestionPacket) {
    const element: ElementLessonPacket = this.editedLesson.elementLessons[idElementLesson];
    element.questionPackets.push(question);
  }

  deleteElement(id: number) {
    this.editedLesson.elementLessons.splice(id, 1);
  }

  deleteQuestion(element: ElementLessonPacket, id: number) {
    element.questionPackets.splice(id, 1);
  }

  addAnswer(question: QuestionPacket, answer: AnswerPacket) {
    question.answerPackets.push(answer);
  }

  deleteAnswer(question: QuestionPacket, id: number) {
    question.answerPackets.splice(id, 1);
  }

  deleteLesson(id: number) {
    this.getEditedCourse().lessonCoursePackets.splice(id, 1);
  }

  getPreviousLessonIndex(indexTabLesson: number): LessonCoursePacket {
    // poprzednia odblokowana
    // const index: number = this.getEditedCourse().lessonCoursePackets.findIndex(value => );
    const type = this.getEditedCourse().lessonCoursePackets[indexTabLesson].type;
    const lessonPrevious = this.getEditedCourse().lessonCoursePackets.find((value, index) => index + 1 === indexTabLesson && value.type === type);
    return lessonPrevious;
  }

  getPreviousLesson(): LessonCoursePacket {
    const indexTabLesson = this.getEditedCourse().lessonCoursePackets.findIndex(value => value === this.getEditedLesson());
    // poprzednia odblokowana
    // const index: number = this.getEditedCourse().lessonCoursePackets.findIndex(value => );
    const type = this.getEditedCourse().lessonCoursePackets[indexTabLesson].type;
    const lessonPrevious = this.getEditedCourse().lessonCoursePackets.find((value, index) => index + 1 === indexTabLesson && value.type === type);
    return lessonPrevious;
  }

  getNextLesson(): LessonCoursePacket {
    const indexTabLesson = this.getEditedCourse().lessonCoursePackets.findIndex(value => value === this.getEditedLesson());
    return this.getEditedCourse().lessonCoursePackets.find((value, index) => index === indexTabLesson + 1);
  }



  getLessonUserLoggedNextLesson(): LessonUserPacket {
    return this.getEditedLesson().lessonUsers.find(value => value.username === this.user.getUserLogged().username);
  }

  addEditedLessonToEditedCourses() {
    const id: number = this.editedCourse.lessonCoursePackets.findIndex(value => value === this.getEditedLesson());
    if (id === -1) {
      this.editedCourse.lessonCoursePackets.push(this.getEditedLesson());
    } else {
      this.editedCourse.lessonCoursePackets[id] = this.getEditedLesson();
    }
  }

  swapLesson(id1: number, id2: number) {
    if (this.getEditedCourse().lessonCoursePackets[id1] && this.getEditedCourse().lessonCoursePackets[id2]) {
      const tmp: LessonCoursePacket = this.getEditedCourse().lessonCoursePackets[id1];
      this.getEditedCourse().lessonCoursePackets[id1] = this.getEditedCourse().lessonCoursePackets[id2];
      this.getEditedCourse().lessonCoursePackets[id2] = tmp;
    }
  }

  getStringIfToUnlock(lesson: LessonCoursePacket): string {
    if (lesson.idNext === 1) {
      return 'Poprzednia';
    } else if (lesson.idNext === 2) {
      return 'Poprzednia/zakup';
    } else if (lesson.idNext === 3) {
      return 'Zakup';
    } else {
      return 'Brak';
    }
  }

  getStringCategoryLesson(lesson: LessonCoursePacket): string {
    if (lesson.type === 1) {
      return 'Lekcja';
    } else if (lesson.type === 2) {
      return 'Ciekawostka';
    } else {
      return 'Sprawdzian';
    }
  }


}
