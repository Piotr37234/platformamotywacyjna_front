import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrzeglajKursyComponent } from './przegladaj-kursy.component';

describe('PrzeglajKursyComponent', () => {
  let component: PrzeglajKursyComponent;
  let fixture: ComponentFixture<PrzeglajKursyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrzeglajKursyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrzeglajKursyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
