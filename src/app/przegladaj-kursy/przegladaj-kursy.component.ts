import {Component, OnInit} from '@angular/core';
import {CourseToolsService} from '../course-tools.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CategoryPacket, CoursePacket, UserPacket} from '../dataPacket';
import {EditorToolsService} from '../editor-tools.service';

@Component({
  selector: 'app-przeglaj-kursy',
  templateUrl: './przegladaj-kursy.component.html',
  styleUrls: ['./przegladaj-kursy.component.css']
})
export class PrzeglajKursyComponent implements OnInit {

  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private activateRouter: ActivatedRoute, private router: Router, private editor: EditorToolsService) {
  }

  textSearch: string;
  checkNazwyKursow: boolean;
  checkAutorzy: boolean;
  checkOpisy: boolean;
  courses: CoursePacket[] = [];
  cousersUserLogged: CoursePacket[] = [];
  courseCategory: any = [];
  mode: string;
  filterCurs: CategoryPacket;
  passwordField: string;

  ngOnInit() {

    this.mode = this.activateRouter.snapshot.params['mode'];

    this.courseToolsService.getAllCourses().subscribe(value => {
      this.courses = value;
    });

    this.courseToolsService.getAllCategory().subscribe(value => {
      this.courseCategory = value;
    });
    this.checkNazwyKursow = true;
    console.log(this.courses);
    // console.log(this.user.getUserLogged());
    this.sortCourses();
    console.log(this.getCourseDisplay());
  }

  search() {
    if (this.textSearch !== '') {
      if (this.checkNazwyKursow) {
        this.searchName();
      }
      if (this.checkAutorzy) {
        this.searchAuthors();
      }
      if (this.checkOpisy) {
        this.searchDescription();
      }
      this.sortCourses();
    } else if (this.textSearch === '') {
      this.ngOnInit();

    }
  }

  saveUserToCourse(id: number) {

    let ok = true;

    this.courseToolsService.getCourse(id).subscribe(
      value => {
        if (this.isPassword(value)) {
          ok = false;
          if (this.passwordField === value.password) {
            ok = true;
          }
        }

        if (ok) {

          if (value.cost !== 0) {

            if (this.user.getUserLogged().money < value.cost) {
              ok = false;
            } else {
              this.user.addMoney(this.user.getUserLogged().id, - value.cost);
            }

          }
        }

        if (ok) {
          this.courseToolsService.saveUserLoggedToCourses(value).subscribe(value1 => {
            this.courseToolsService.getCourse(id).subscribe(value2 => {
              this.courseToolsService.setSelectedCourse(value2);
              this.router.navigate(['/wybrany-kurs']);
            });
          });
        }


      });
    return false;


  }

  continueStudy(id: number) {
    this.courseToolsService.getCourse(id).subscribe(
      value => {
        this.courseToolsService.setSelectedCourse(value);
        this.router.navigate(['/wybrany-kurs']);
      });
  }

  startEdit(id: number) {
    this.courseToolsService.getCourse(id).subscribe(
      value => {
        this.editor.setEditedCourse(value);
        this.router.navigate(['/edytor-kurs']);
      });
  }


  // updateKategory() {
  //   for (let i = 0; i < this.testData.length; i++) {
  //     if(this.kategory.find(value => this.testData[i].kategory))
  //     this.kategory.push(this.testData[i].kategory);
  //
  //   }
  // }
  searchDescription() {
    this.courses = this.courses.filter(res => {
      return res.description.toLocaleLowerCase().match(this.textSearch.toLocaleLowerCase());
    });
  }

  searchAuthors() {
    this.courses = this.courses.filter(res => {
      return res.autor.username.toLocaleLowerCase().match(this.textSearch.toLocaleLowerCase());
    });
  }

  searchName() {
    this.courses = this.courses.filter(res => {
      return res.name.toLocaleLowerCase().match(this.textSearch.toLocaleLowerCase());
    });
  }

  clickFilterCategories(courseCategoryPacket: CategoryPacket) {
    // this.ngOnInit();
    this.filterByCategories(courseCategoryPacket);
  }

  refreshCourses() {
    this.ngOnInit();
  }

  filterByCategories(courseCategory: any) {
    this.courses = this.courses.filter(res => {
      return res.category.name.match(courseCategory.name);
    });
  }

  filterBySetCategories() {
    this.courses = this.courses.filter(res => {
      return res.category.name.match(this.filterCurs.name);
    });
  }

  setFilterCategories(courseCategory: any) {
    this.filterCurs = courseCategory;
  }

  sortByRate() {
    this.courses = this.courses.sort((a, b) => {
      return b.rate - a.rate;
    });
  }

  sortByRank() {
    this.courses = this.courses.sort((a, b) => {
      return b.rank - a.rank;
    });
  }

  sortCourses() {
    this.courses = this.courses.sort((a, b) => {
      return b.rank - a.rank;
    }).sort((a, b) => {
      return b.rate - a.rate;
    });
  }

  isBronze(course: any): boolean {
    if (course.rank === 1) {
      return true;
    } else {
      return false;
    }
  }

  isSilver(course: any): boolean {
    if (course.rank === 2) {
      return true;
    } else {
      return false;
    }
  }

  isGold(course: any): boolean {
    if (course.rank === 3) {
      return true;
    } else {
      return false;
    }
  }

  getCourseDisplay(): CoursePacket[] {
    if (this.mode === 'przegladaj') {
      return this.getAllCourses();
    } else if (this.mode === 'mojeKursy') {
      return this.getCoursesCreateUserLogged();
    } else if (this.mode === 'rozpoczete') {
      return this.getCoursesSaveUserLogged();
    } else {
      return null;
    }
  }

  getCoursesCreateUserLogged(): CoursePacket[] {
    return this.courseToolsService.getCoursesCreateUserLogged(this.courses);
  }

  getCoursesSaveUserLogged(): CoursePacket[] {
    return this.courseToolsService.getCoursesSaveUserLogged(this.courses);
  }

  getAllCourses(): CoursePacket[] {
    return this.courses;
  }

  isBrowsingCourses(): boolean {
    if (this.mode === 'przegladaj') {
      return true;
    } else {
      return false;
    }
  }

  isDisplayMyCoursesCourses(): boolean {
    if (this.mode === 'mojeKursy') {
      return true;
    } else {
      return false;
    }
  }

  isDisplayMyBeginningCourse(): boolean {
    if (this.mode === 'rozpoczete') {
      return true;
    } else {
      return false;
    }
  }

  isUserLoggedSavedCourse(coursePacket: CoursePacket): boolean {
    return this.courseToolsService.isUserLoggedSavedCourse(coursePacket);
  }

  isUserLoggedCreateCourses(coursePacket: CoursePacket): boolean {
    return this.courseToolsService.isUserLoggedCreateCourses(coursePacket);
  }

  getCourseUserStatus(coursePacket: CoursePacket): number {
    if (this.isUserLoggedCreateCourses(coursePacket)) {
      return 2;
    } else if (this.isUserLoggedSavedCourse(coursePacket)) {
      return 1;
    } else {
      return 0;
    }
  }

  isPassword(course: CoursePacket): boolean {
    if (course.password !== '') {
      return true;
    } else {
      return false;
    }
  }

  isPay(course: CoursePacket): boolean {
    if (course.cost !== 0) {
      return true;
    } else {
      return false;
    }
  }

}

// id: number, name: string, description: string, rate: number, password: string, cost: number
