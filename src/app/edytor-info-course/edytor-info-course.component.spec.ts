import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdytorInfoCourseComponent } from './edytor-info-course.component';

describe('EdytorInfoCourseComponent', () => {
  let component: EdytorInfoCourseComponent;
  let fixture: ComponentFixture<EdytorInfoCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdytorInfoCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdytorInfoCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
