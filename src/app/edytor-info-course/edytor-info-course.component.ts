import {Component, Input, OnInit} from '@angular/core';
import {CourseToolsService} from '../course-tools.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {EditorToolsService} from '../editor-tools.service';
import {CategoryPacket, CoursePacket, LessonCoursePacket, LessonUserPacket} from '../dataPacket';

@Component({
  selector: 'app-edytor-info-course',
  templateUrl: './edytor-info-course.component.html',
  styleUrls: ['./edytor-info-course.component.css']
})
export class EdytorInfoCourseComponent implements OnInit {
  file: File;
  files: FileList;
  objDate = Date.now();
  rank: string;
  category: string;
  categoryPacket: CategoryPacket[] = [];
  test: string[];
  requiredPassword: boolean;
  requiredCost: boolean;
  categoryCreated: boolean;
  categoryNew: string;
  edit: boolean;

  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private router: Router, private edytor: EditorToolsService) {
    this.categoryCreated = false;
  }

  ngOnInit() {
    this.courseToolsService.getAllCategory().subscribe(value => {
      this.categoryPacket = value;
      if (this.categoryPacket.length !== 0) {
        this.category = value[0].name;
      }
    });

    this.rank = 'Braz (darmowa)';
    console.log(this.edytor.getEditedCourse());
    if (this.getEditedCourse().cost > 0) {
      this.requiredCost = true;
    } else {
      this.getEditedCourse().cost = 0;
    }

    if (this.getEditedCourse().password !== '') {
      this.requiredPassword = true;
    } else {
      this.getEditedCourse().password = '';
    }
  }

  setCategoryCourse(categoryNew: string) {
    this.categoryCreated = true;
    this.category = categoryNew;
  }

  getStandardLessonEditedCourse(): any {
    return this.edytor.getArrayStandardLessonEditedCourse();
  }

  getExtraLessonEditedCourse(): any {
    return this.edytor.getArrayExtraLessonEditedCourse();
  }

  getTestLessonEditedCourse(): any {
    return this.edytor.getArrayTestLessonEditedCourse();
  }

  getDataLoggedUserAboutLesson(lessonCoursePacket: LessonCoursePacket): LessonUserPacket {
    return this.courseToolsService.getLessonUserLoggedUser(lessonCoursePacket);
  }

  getEditedCourse(): CoursePacket {
    return this.edytor.getEditedCourse();
  }

  setEditedLessonCourse(LessonCourse: any) {
    this.courseToolsService.setSelectedLessonCourse(LessonCourse);
    this.router.navigate(['/lekcja-kurs']);
  }


  editLesson() {
    this.router.navigate(['edytor-lekcja']);
  }

  deleteLesson() {
  }

  isBronze(course: any): boolean {
    if (course.rank === 1) {
      return true;
    } else {
      return false;
    }
  }

  isSilver(course: any): boolean {
    if (course.rank === 2) {
      return true;
    } else {
      return false;
    }
  }

  isGold(course: any): boolean {
    if (course.rank === 3) {
      return true;
    } else {
      return false;
    }
  }

  getFiles(e) {
    // e.target.files.set(this.file);
    console.log(this.files);
  }

  saveCourse() {

    let ok = true;


    if (this.rank === 'Srebro (Wymagana opłata 1000 punktów)') {
      if (this.user.getUserLogged().money < 1000) {
        ok = false;
      } else {
        this.user.addMoney(this.user.getUserLogged().id, -1000);
      }
    }

    if (this.rank === 'Złoto (Wymagana opłata 3000 punktów)') {
      if (this.user.getUserLogged().money < 3000) {
        ok = false;
      } else {
        this.user.addMoney(this.user.getUserLogged().id, -3000);
      }
    }

    if(ok) {
      this.setRank();
      this.setCategory();
      if (this.edit) {
        this.edytor.updateSelectedCourse().subscribe(value => {
            this.edytor.setEditedCourse(value);
          }
        );
      } else {
        this.edytor.createSelectedCourse().subscribe(value => {
            this.edytor.setEditedCourse(value);
            console.log('id' + this.getEditedCourse().id);
          }
        );
      }


      this.user.getUserLogged().createCurses++;
      this.user.updateUser().subscribe();
      this.router.navigate(['/edytor-kurs']);

    }
  }


  setRank() {
    console.log(this.rank);
    if (this.rank === 'Braz (darmowa)') {
      this.edytor.getEditedCourse().rank = 1;
    }
    if (this.rank === 'Srebro (Wymagana opłata 1000 punktów)') {
      this.edytor.getEditedCourse().rank = 2;
    }
    if (this.rank === 'Złoto (Wymagana opłata 3000 punktów)') {
      this.edytor.getEditedCourse().rank = 3;
    }
  }

  setCategory() {
    console.log('to');
    console.log(this.category);
    this.categoryPacket.forEach(value => {
      if (value.name === this.category) {
        this.getEditedCourse().category = value;
      }
    });
    console.log(this.edytor.getEditedCourse());
  }


  isExist(): boolean {
    if (typeof this.getEditedCourse().id === 'undefined') {
      return false;
    } else {
      return true;
    }
  }

  getIdCourseEdit(): number {
    return this.getEditedCourse().id;
  }


  deleteCourse(id: number) {
    this.edytor.deleteCourse(id).subscribe();
    this.router.navigate(['konto']);
  }

  addCategory() {
    const categoryPacket: CategoryPacket = new class implements CategoryPacket {
      id: number;
      name: string;
    };
    categoryPacket.name = this.categoryNew;
    this.categoryPacket.push(categoryPacket);
    this.category = categoryPacket.name;
  }

}
