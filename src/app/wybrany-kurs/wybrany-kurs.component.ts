import {Component, OnInit} from '@angular/core';
import {CourseToolsService} from '../course-tools.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {LessonCoursePacket, LessonUserPacket} from '../dataPacket';

@Component({
  selector: 'app-wybrany-kurs',
  templateUrl: './wybrany-kurs.component.html',
  styleUrls: ['./wybrany-kurs.component.css']
})
export class WybranyKursComponent implements OnInit {
  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private router: Router) {
  }

  ngOnInit() {

    console.log(this.user.getUserLogged());
    console.log(this.courseToolsService.getSelectCourse());
    // console.log(this.getLessonUserLoggedUser(this.getStandardLessonSelectedCourse()[1]));
    console.log('standard');
    console.log(this.getStandardLessonSelectedCourse());
    console.log(this.getDataLoggedUserAboutLesson(this.getStandardLessonSelectedCourse()[0]).unlocked);
    // console.log(this.getLessonUserLoggedUser());
    // this.testUsers = [
    //   {
    //     'id': 1,
    //     'username': 'Jan',
    //     'password': 'xxx',
    //     'money': 123
    //   }
    // ];
    // this.testCategories = [
    //   {
    //     'id': 1,
    //     'name': 'Informatyka',
    //   },
    //   {
    //     'id': 2,
    //     'name': 'Biologia',
    //   },
    //   {
    //     'id': 3,
    //     'name': 'Astronomia',
    //   },
    //   {
    //     'id': 4,
    //     'name': 'Fizyka',
    //   }
    // ];
    // this.testData = [
    //   {
    //     'id': 1,
    //     'name': 'Java',
    //     'description': 'Kurs o podstawach Javy',
    //     'rate': 2,
    //     'password': '',
    //     'cost': 0,
    //     'rank': 1,
    //     'img': 'src/app/img/j.jpeg',
    //     'user': this.testUsers[0],
    //     'courseCategory': this.testCategories[0]
    //   }];
    // this.testStandardLessonCourse = [
    //   {
    //     'id': 1,
    //     'name': 'Wstep do Programowania',
    //     'cost': 0,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 2,
    //     'name': 'Pierwszy program',
    //     'cost': 0,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 3,
    //     'name': 'Zmienne',
    //     'cost': 0,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 4,
    //     'name': 'Funkcje',
    //     'cost': 0,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 5,
    //     'name': 'Dziedziczenie',
    //     'cost': 0,
    //     'course': this.testData[0]
    //   }
    // ];
    // this.testExtraLessonCourse = [
    //   {
    //     'id': 1,
    //     'name': 'Przepełnienie tablicy a wirusy komputerowe',
    //     'cost': 120,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 2,
    //     'name': 'Wskazniki',
    //     'cost': 250,
    //     'course': this.testData[0]
    //   },
    // ];
    // this.testTestLessonCourse = [
    //   {
    //     'id': 1,
    //     'name': 'Sprawdzian 1',
    //     'cost': 250,
    //     'course': this.testData[0]
    //   },
    //   {
    //     'id': 2,
    //     'name': 'Sprawdzian 2',
    //     'cost': 210,
    //     'course': this.testData[0]
    //   },
    // ];
    // this.testStandardLessonUser = [
    //   {
    //     'id': 1,
    //     'lessonCourse' : this.testStandardLessonCourse[0],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': true,
    //     'result': 0
    //   },
    //   {
    //     'id': 2,
    //     'lessonCourse' : this.testStandardLessonCourse[1],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   {
    //     'id': 3,
    //     'lessonCourse' : this.testStandardLessonCourse[2],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   {
    //     'id': 4,
    //     'lessonCourse' : this.testStandardLessonCourse[3],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   {
    //     'id': 5,
    //     'lessonCourse' : this.testStandardLessonCourse[4],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    // ];
    // this.testExtraLessonUser = [
    //   {
    //     'id': 1,
    //     'lessonCourse' : this.testExtraLessonCourse[0],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   {
    //     'id': 2,
    //     'lessonCourse' : this.testExtraLessonCourse[1],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   ];
    // this.testTestLessonUser = [
    //   {
    //     'id': 6,
    //     'lessonCourse' : this.testTestLessonCourse[0],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    //   {
    //     'id': 7,
    //     'lessonCourse' : this.testTestLessonCourse[1],
    //     'user': this.testUsers[0],
    //     'completed': false,
    //     'unlock': false,
    //     'result': 0
    //   },
    // ];
  }

  getStandardLessonSelectedCourse(): any {
    return this.courseToolsService.getArrayStandardLessonSelectedCourse();
  }
  getExtraLessonSelectedCourse(): any {
    return this.courseToolsService.getArrayExtraLessonSelectedCourse();
  }
  getTestLessonSelectedCourse(): LessonCoursePacket[] {
    return this.courseToolsService.getArrayTestLessonSelectedCourse();
  }
  getDataLoggedUserAboutLesson(lessonCoursePacket: LessonCoursePacket): LessonUserPacket {
    return this.courseToolsService.getLessonUserLoggedUser(lessonCoursePacket);
  }
  setSelectedLessonCourse(LessonCourse: any) {
    this.courseToolsService.setSelectedLessonCourse(LessonCourse);
    this.router.navigate(['/lekcja-kurs']);
  }

  buy(idLesson: number, cost: number) {
     if (this.user.getUserLogged().money >= cost) {
      this.user.addLoggedUserMoney(-cost).subscribe();
      this.courseToolsService.modifyLessonUser(idLesson,-1,-1,-1,-1,1,-1).subscribe();
      this.ngOnInit();
     }
  }
  // getWindowStandardLesson(): LessonUser[] {
  //   return this.testLessonUser.filter(value => value.id < 6);
  // }
  //
  // getWindowExtraLesson(): LessonUser[] {
  //   return this.testLessonUser.filter(value => value.id > 6 &&  value.id < 8 );
  // }
  //
  // getWindowTestLesson(): LessonUser[] {
  //   return this.testLessonUser.filter(value => value.id > 8 );
  // }
}

// export interface StandardLessonCourse extends LessonCourse {
//   id: number;
//   lesson: Lesson;
//   course: Course;
// }
