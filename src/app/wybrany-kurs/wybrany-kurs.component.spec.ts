import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WybranyKursComponent } from './wybrany-kurs.component';

describe('WybranyKursComponent', () => {
  let component: WybranyKursComponent;
  let fixture: ComponentFixture<WybranyKursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WybranyKursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WybranyKursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
