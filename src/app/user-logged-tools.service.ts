import {Injectable} from '@angular/core';
import {AuthenticationService} from './tmp/authentication.service';
import {HttpClient} from '@angular/common/http';
import {DataService} from './data.service';
import {UserPacket} from './dataPacket';

@Injectable({
  providedIn: 'root'
})
export class UserLoggedToolsService {
  private userLogged: UserPacket;

  constructor(private authentication: AuthenticationService, private dataService: DataService) {
    this.userLogged = null;
  }

  tmp() {
    let user: UserPacket = new class implements UserPacket {
      completedCourse: number;
      correctAnswer: number;
      createCurses: number;
      effectiveness: number;
      errorAnswer: number;
      experience: number;
      id: number;
      img: string;
      money: number;
      password: string;
      username: string;
    };
    user.id = 1;
    user.username = 'Jan';
    user.money = 200;
    user.password = '123';
    user.img = 'm';
    this.setUserLogged(user);
  }

  setUserLogged(user: UserPacket) {
    this.userLogged = user;
  }

  getUserLogged() {
    return this.userLogged;
  }

  outlogged() {
    this.userLogged = null;
  }

  isLogged() {
    if (this.userLogged === null) {
      return false;
    } else {
      return true;
    }
  }

  logged(username: string, password: string) {
    return this.dataService.getAccountUser(username, password);
  }

  addMoney(idUser: number, value: number) {
    this.userLogged.money = this.userLogged.money + value;
    return this.dataService.addMoney(idUser, value);
  }

  addLoggedUserMoney(value: number) {
    this.userLogged.money = this.userLogged.money + value;
    return this.dataService.addMoney(this.getUserLogged().id, value);
  }

  updateUser() {
    return this.dataService.createUser(this.userLogged);
  }

  createUser(userPacket: UserPacket) {
    return this.dataService.createUser(userPacket);
  }

  getAllUsers() {
    return this.dataService.getAllUsers();
  }


  // createAccount(username: String, password: String, img: String){
  //
  // }
}
