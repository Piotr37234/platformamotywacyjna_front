import {Component, OnInit} from '@angular/core';
import {Usluga} from './tmp/Usluga';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor() {
  }

  ngOnInit(): void {
  }

  // hello() {   this._dane_ze_strony.getHello().subscribe(response => {
  //     console.log(response);
  //   });
  // }

  setUser() {
    // this._dane_ze_strony.getUser()
    //   .subscribe((data: User) => this.user = {
    //     id: data['id'],
    //     username: data['username'],
    //     password: data['password'],
    //     money: data['money']
    //   });
  }

  // getAllUsers() {
  //   this.poletekstowe = this._dane_ze_strony.getAllUsers();
  // }
}


// export interface User {
//   id: number;
//   username: string;
//   password: string;
//   money: number;
// }


// export interface Course {
//   id: number;
//   name: string;
//   description: string;
//   rate: number;
//   password: string;
//   cost: number;
//   rank: number;
//   img: string;
//   user: User;
//   courseCategory: CourseCategory;
// }


// export interface CourseCategory {
//   id: number;
//   name: string;
// }


// export interface Lesson {
//   id: number;
//   name: string;
//   cost: number;
// }

// export interface LessonCourse {
//   id: number;
//   name: string;
//   cost: number;
//   course: Course;
// }

// export interface StandardLessonCourse extends LessonCourse {
//   id: number;
// }
//
// export interface ExtraLessonCourse extends LessonCourse {
//   id: number;
// }
//
// export interface TestLessonCourse extends LessonCourse {
//   id: number;
// }

// export interface LessonUser {
//   id: number;
//   lessonCourse: LessonCourse;
//   user: User;
//   completed: boolean;
//   unlock: boolean;
//   result: number;
//   type: number;
// }

// export interface StandardLessonUser extends LessonUser {
//   id: number;
// }
//
// export interface ExtraLessonUser extends LessonUser {
//   id: number;
// }
//
// export interface TestLessonUser extends LessonUser {
//   id: number;
// }

// export interface ElementLesson {
//   id: number;
//   name: string;
//   header: string;
//   content: string;
//   type: number;
// }


// export interface Question {
//   id: number;
//   elementLesson: ElementLesson;
//   questionContent: QuestionContent;
// }
//
// export interface QuestionContent {
//   id: number;
//   content: string;
// }
//
// export interface QuestionVariant {
//   id: number;
//   variant: Variant;
//   questionContent: QuestionContent;
// }



// export interface Variant {
//   id: number;
//   content: string;
//   good: number;
// }


// export interface LessonCourseElement {
//   id: number;
//   lessonCourse: LessonCourse;
//   elementLesson: ElementLesson;
// }
