import {Component, OnInit} from '@angular/core';
import {CourseToolsService} from '../course-tools.service';
import {
  AnswerPacket,
  ElementLessonPacket,
  LessonCoursePacket, LessonUserPacket, ModifyLessonUserPacket,
  OdpPacket,
  QuestionPacket,
  ResultElementLesson, ResultLesson
} from '../dataPacket';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-lekcja-kurs',
  templateUrl: './lekcja-kurs.component.html',
  styleUrls: ['./lekcja-kurs.component.css']
})
export class LekcjaKursComponent implements OnInit {
  active_card_index: number;

  // answerOdp: AnswerOdp[];
  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private router: Router) {
  }

  ngOnInit() {
    this.active_card_index = 0;
  }

  getData() {
    // return this.data;
  }

  getNumber() {
    return 2;
  }

  getArrayElementSelectedCourseLesson(): ElementLessonPacket[] {
    return this.courseToolsService.getArrayElementSelectedCourseLesson();
  }

  getActiveCard(): ElementLessonPacket {
    return this.courseToolsService.getElementSelectedCourseLesson(this.active_card_index);
  }

  clikCard(id: number) {
    this.active_card_index = id - 1;
  }

  setClassActiveCard(id: number): string {
    if (id === this.active_card_index) {
      return 'nav-link active';
    }
    return 'nav-link';
  }

  getArrayQuestionsActiveCard(): QuestionPacket[] {
    return this.courseToolsService.getArrayQuestionsElementLesson(this.getActiveCard());
  }

  getArrayAnswerQuestion(question: any): AnswerPacket[] {
    return this.courseToolsService.getArrayAnswerQuestion(question);
  }

  isActiveIsElementQuestion(e): boolean {
    return this.courseToolsService.isElementQuestion(this.getActiveCard());
  }

  checkOdpElement(elementLessonPacket: ElementLessonPacket): ResultElementLesson {
    return this.courseToolsService.checkOdpElement(elementLessonPacket);
  }

  Commit() {
    console.log(this.checkOdpElement(this.getActiveCard()));
  }

  getDataLoggedUserAboutLessonSelectLessonCourse(): LessonUserPacket {
    return this.courseToolsService.getDataLoggedUserAboutLessonSelectLessonCourse();
  }

  getSelectedLessonCourse(): LessonCoursePacket {
    return this.courseToolsService.getSelectedLessonCourse();
  }

  getStringTypeSelectedLessonCourse(): string {
    return this.courseToolsService.getStringTypeSelectedLessonCourse();
  }

  getStatusColor(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'alert alert-success';
    } else {
      return 'alert alert-warning';
    }
  }


  unlockIfPossibilty() {
    if (typeof this.courseToolsService.getNextLesson() !== 'undefined') {
      if (this.courseToolsService.getNextLesson().type === 1) {
        console.log('idNext' + this.courseToolsService.getNextLesson().idNext);
        if (this.courseToolsService.getNextLesson().idNext === 1 || this.courseToolsService.getNextLesson().idNext === 2) {
          this.courseToolsService.getLessonUserLoggedUserNextLesson().unlocked = 1;
        }
      }
    }
  }


  check() {
    const resultLesson: ResultLesson = this.courseToolsService.checkOdpLessonCourse(this.getSelectedLessonCourse());
    console.log(resultLesson);
    const lessonUserPacket: LessonUserPacket = this.getDataLoggedUserAboutLessonSelectLessonCourse();
    lessonUserPacket.correctQuestion += resultLesson.correctQuestion;
    this.user.getUserLogged().correctAnswer += resultLesson.correctQuestion;
    lessonUserPacket.errorQuestion += resultLesson.errorQuestion;
    this.user.getUserLogged().errorAnswer += resultLesson.errorQuestion;
    this.user.getUserLogged().experience++;
    this.user.getUserLogged().effectiveness = this.user.getUserLogged().correctAnswer / (this.user.getUserLogged().correctAnswer + lessonUserPacket.errorQuestion) * 100;
    lessonUserPacket.result = lessonUserPacket.correctQuestion / (lessonUserPacket.correctQuestion + lessonUserPacket.errorQuestion) * 100;
    if (resultLesson.completed === true) {
      lessonUserPacket.completed = 1;
    }
    if (resultLesson.completed === false) {
      lessonUserPacket.completed = 0;
    }
    if (lessonUserPacket.completed) {
      this.user.addLoggedUserMoney(this.getPktMaxLoggedUserForSelectedLessonCourse()).subscribe();
      this.unlockIfPossibilty();
    }
    lessonUserPacket.trials++;
    this.courseToolsService.modifyLessonUser(lessonUserPacket.id, lessonUserPacket.result, lessonUserPacket.correctQuestion, lessonUserPacket.errorQuestion, lessonUserPacket.trials, lessonUserPacket.unlocked, lessonUserPacket.completed).subscribe();
    this.updateSelectedCourse().subscribe();
    this.user.updateUser().subscribe();
  }

  clickCommit() {
    const lessonUserPacket: LessonUserPacket = this.getDataLoggedUserAboutLessonSelectLessonCourse();
    if (!lessonUserPacket.completed) {
      this.check();
    } else {
      this.router.navigate(['wybrany-kurs']);
    }
  }


  updateSelectedCourse() {
    return this.courseToolsService.updateSelectedCourse();
  }


  getStringStatus(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'lekcja ukończona';
    } else {
      return 'w trakcie';
    }
  }

  getButtonStringZatwierdz(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'Zakoncz';
    } else {
      return 'Zatwierdz wszystkie pytania i sprawdz';
    }
  }

  getPktMaxLoggedUserForSelectedLessonCourse(): number {
    return this.courseToolsService.getPktMaxLoggedUserForSelectedLessonCourse();
  }


// saveElementLessonActiveCardToOdpPacket(): OdpPacket {
//
// }
//   getArrayWariantsQuestion(): Wariants[] {
//
//   }
}


