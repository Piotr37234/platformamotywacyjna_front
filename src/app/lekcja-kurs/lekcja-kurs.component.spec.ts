import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LekcjaKursComponent } from './lekcja-kurs.component';

describe('LekcjaKursComponent', () => {
  let component: LekcjaKursComponent;
  let fixture: ComponentFixture<LekcjaKursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LekcjaKursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LekcjaKursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
