import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelLogowaniaComponent } from './panel-logowania.component';

describe('PanelLogowaniaComponent', () => {
  let component: PanelLogowaniaComponent;
  let fixture: ComponentFixture<PanelLogowaniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelLogowaniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelLogowaniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
