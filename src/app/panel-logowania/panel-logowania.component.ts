import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {e} from '@angular/core/src/render3';
import {AuthenticationService} from '../tmp/authentication.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {CourseToolsService} from '../course-tools.service';

@Component({
  selector: 'app-panel-logowania',
  templateUrl: './panel-logowania.component.html',
  styleUrls: ['./panel-logowania.component.css']
})
export class PanelLogowaniaComponent implements OnInit {
  constructor(private router: Router, private user: UserLoggedToolsService, private courseToolsService: CourseToolsService) {
  }

  ngOnInit() {
  }
  // zalogujKlik(e) {
  //   const username = e.target.querySelector('#username').value;
  //   const password = e.target.querySelector('#password').value;
  //
  //   console.log(username, password);
  //   if (username === 'konto' && password === 'konto') {
  //     this.user.setUserLoggedIn();
  //     this.router.navigate(['konto']);
  //   }
  //   return false;
  // }
  zalogujKlik(e) {
    const username = e.target.querySelector('#username').value;
    const password = e.target.querySelector('#password').value;

   //  this.user.getAccountUser(username, password).subscribe((response: User) => {
   //   // console.log(response);
   //     if (response != null) {
   //      this.user.setUserLoggedIn(response);
   //      this.router.navigate(['konto']);
   //     }
   // });

    this.user.logged(username, password).subscribe(value => {
      if (value != null) {
        this.user.setUserLogged(value);
        this.router.navigate(['konto']);
      }
    });

      // console.log(this.user.getUserLogged().username);
    // if (this.us) {
    //   this.user.setUserLoggedIn();
    //   this.router.navigate(['konto']);
    // }
    return false;
  }
}
