import {Component, OnInit} from '@angular/core';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {UserPacket} from '../dataPacket';
import {CourseToolsService} from '../course-tools.service';
import {EditorToolsService} from '../editor-tools.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-konto',
  templateUrl: './konto.component.html',
  styleUrls: ['./konto.component.css']
})
export class KontoComponent implements OnInit {
  username;

  constructor(private user: UserLoggedToolsService, private courseToolsService: CourseToolsService, private editor: EditorToolsService, private router: Router) {
  }

  ngOnInit() {
    // const objUser: User = JSON.parse(localStorage.getItem('currentUser'));
    this.username = this.user.getUserLogged().username;
  }

  getUserLogged(): UserPacket {
    return this.user.getUserLogged();
  }

  wyloguj() {
    this.user.outlogged();
  }

  getRank() {
    if (this.getUserLogged().experience < 10) {
      return 'amator';
    }
    else if (this.getUserLogged().experience < 50) {
      return 'zawodowiec';
    } else {
      return 'profesjonalista';
    }
  }

  getExperienceNextRand() {
    if (this.getUserLogged().experience < 10) {
      return 10 - this.getUserLogged().experience;
    } else {
      return 50 - this.getUserLogged().experience;
    }
  }

  newCourse() {
    this.editor.loadNewCourse();
    this.router.navigate(['edytor-info']);
  }


  getProcent() {
    if (this.getUserLogged().experience < 10) {
      return (this.getUserLogged().experience / 10) * 100;
    } else {
      return (this.getUserLogged().experience / 50) * 100;
    }
  }

  getStyleProcent() {
    return {width: this.getProcent() + '%'};
  }


}
