import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {Usluga} from './tmp/Usluga';
import {RouterModule, Routes} from '@angular/router';
import { KontoComponent } from './konto/konto.component';
import { PanelLogowaniaComponent } from './panel-logowania/panel-logowania.component';
import { NaglowekComponent } from './naglowek/naglowek.component';
import { StopkaComponent } from './stopka/stopka.component';
import { PanelRejestracjiComponent } from './panel-rejestracji/panel-rejestracji.component';
import { StronaGlownaComponent } from './strona-glowna/strona-glowna.component';
import { PrzeglajKursyComponent } from './przegladaj-kursy/przegladaj-kursy.component';
import { WybranyKursComponent } from './wybrany-kurs/wybrany-kurs.component';
import {FormsModule} from '@angular/forms';
import { LekcjaKursComponent } from './lekcja-kurs/lekcja-kurs.component';
import {DataService} from './data.service';
import {CourseToolsService} from './course-tools.service';
import {UserLoggedToolsService} from './user-logged-tools.service';
import { EdytorKursowComponent } from './edytor-kursow/edytor-kursow.component';
import { EdytorLekcjiComponent } from './edytor-lekcji/edytor-lekcji.component';
import { EdytorInfoCourseComponent } from './edytor-info-course/edytor-info-course.component';

// { path: 'konto', component: KontoComponent },

const appRoutes: Routes = [
  { path: 'http://localhost:4200', redirectTo: '/index', pathMatch: 'full'},
  { path: 'index', component: StronaGlownaComponent },
  { path: 'kursy/:mode', component: PrzeglajKursyComponent },
  { path: 'wybrany-kurs', component: WybranyKursComponent },
  { path: 'lekcja-kurs', component: LekcjaKursComponent },
  { path: 'edytor-kurs', component: EdytorKursowComponent },
  { path: 'edytor-lekcja', component: EdytorLekcjiComponent },
  { path: 'edytor-info', component: EdytorInfoCourseComponent },
  // { path: 'konto', canActivate: [AuthguardGuard], component: KontoComponent },
  { path: 'konto', component: KontoComponent },
  { path: '', redirectTo: '/index', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    KontoComponent,
    PanelLogowaniaComponent,
    NaglowekComponent,
    StopkaComponent,
    PanelRejestracjiComponent,
    StronaGlownaComponent,
    PrzeglajKursyComponent,
    WybranyKursComponent,
    LekcjaKursComponent,
    EdytorKursowComponent,
    EdytorLekcjiComponent,
    EdytorInfoCourseComponent,
  ],
  imports: [
    FormsModule, BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes)
  ],

  providers: [DataService, CourseToolsService, UserLoggedToolsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
