  export interface UserPacket {
  id: number;
  username: string;
  password: string;
  money: number;
  img: string;
  completedCourse: number;
  errorAnswer: number;
  correctAnswer: number;
  effectiveness: number;
  createCurses: number;
  experience: number;

}
export interface SaveUserCoursesPacket {
  idUser: number;
  idCourse: number;
}
export interface  QuestionPacket {
  id: number;
  content: string;
  pkt: number;
  answerPackets: AnswerPacket[];
}
export interface LoginPacket {
  username: string;
  password: string;
}
export interface LessonUserPacket {
  id: number;
  completed: number;
  result: number;
  unlocked: number;
  correctQuestion: number;
  errorQuestion: number;
  trials: number;
  type: number;
  username: string;
}
export interface LessonCoursePacket {
  id: number;
  name: string;
  cost: number;
  type: number;
  locked: number;
  elementLessons: ElementLessonPacket[];
  lessonUsers: LessonUserPacket[];
  idNext: number;
}
export interface ElementLessonPacket {
  id: number;
  name: string;
  header: string;
  content: string;
  type: number;
  questionPackets: QuestionPacket[];
}
export interface CoursePacket {
  id: number;
  name: string;
  description: string;
  rate: number;
  password: string;
  cost: number;
  rank: number;
  img: string;
  category: CategoryPacket;
  autor: UserPacket;
  lessonCoursePackets: LessonCoursePacket[];
}
export interface CategoryPacket {
  id: number;
  name: string;
}
export interface AnswerPacket {
  id: number;
  content: string;
  good: boolean;
  odp: boolean;
}
export interface OdpPacket {
  idQuestion: number;
  odp: number[];
}
export interface TransferMoneyPacket {
  idUser: number;
  value: number;
}
export interface ResultElementLesson {
  correctQuestion: number;
  errorQuestion: number;
  pkt: number;
  completed: boolean;
}
export interface ResultLesson {
  correctQuestion: number;
  errorQuestion: number;
  pkt: number;
  completed: boolean;
}
export interface ModifyLessonUserPacket {
    idUser: number;
    idLesson: number;
    unlock: number;
    completed: number;
    correctQuestion: number;
    errorQuestion: number;
    trials: number;
    result: number;
}
