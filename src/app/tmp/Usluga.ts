import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable()
export class Usluga {
  constructor(private http: HttpClient) {
  }
  getAllUsers() {
    return this.http.post('localhost:8080/user/showAllUsers', '');
  }
  getHello() {
    return this.http.get('http://localhost:4200/api/user/hello');
  }
  getHelloSub() {
    this.http.get('localhost:8080/user/hello').subscribe((response) => console.log(response));
  }
  getUser() {
    return this.http.get('http://localhost:4200/api/user/2');
  }
  utworzKonto() {
    return this.http.post('http://localhost:4200/api/user/registry', 'json');
  }
  // addHero (hero: Hero): Observable<Hero> {
  //   return this.http.post<Hero>(this.heroesUrl, hero, httpOptions)
  //     .pipe(
  //       catchError(this.handleError('addHero', hero))
  //     );
  // }
}
