// import {Injectable} from '@angular/core';
// import {
//   // Course,
//   // CourseCategory,
//   // ElementLesson,
//   // ExtraLessonCourse,
//   // ExtraLessonUser,
//   // LessonCourseElement,
//   // Question,
//   // QuestionContent,
//   // QuestionVariant,
//   // StandardLessonCourse,
//   // StandardLessonUser,
//   // TestLessonCourse,
//   // TestLessonUser,
//   // User,
//   // Variant
// } from './app.component';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class TestDataService {
//   // testUsers: User[] = [];
//   // testCategories: CourseCategory[] = [];
//   // testData: Course[] = [];
//   // testStandardLessonUser: StandardLessonUser[] = [];
//   // testExtraLessonUser: ExtraLessonUser[] = [];
//   // testTestLessonUser: TestLessonUser[] = [];
//   // testStandardLessonCourse: StandardLessonCourse[] = [];
//   // testExtraLessonCourse: ExtraLessonCourse[] = [];
//   // testTestLessonCourse: TestLessonCourse[] = [];
//   // testLessonCourseElement: LessonCourseElement[] = [];
//   // testElementLesson: ElementLesson[] = [];
//   // testQuestionContent: QuestionContent[] = [];
//   // testQuestionVariants: QuestionVariant[] = [];
//   // testVariant: Variant[] = [];
//   // testQuestion: Question[] = [];
//
//   constructor() {
//     this.testUsers = [
//       {
//         'id': 1,
//         'username': 'Jan',
//         'password': 'xxx',
//         'money': 123
//       }
//     ];
//
//     this.testCategories = [
//       {
//         'id': 1,
//         'name': 'Informatyka',
//       },
//       {
//         'id': 2,
//         'name': 'Biologia',
//       },
//       {
//         'id': 3,
//         'name': 'Astronomia',
//       },
//       {
//         'id': 4,
//         'name': 'Fizyka',
//       }
//     ];
//
//     this.testData = [
//       {
//         'id': 1,
//         'name': 'Astronomia',
//         'description': 'Kurs o podstawach astronomii',
//         'rate': 2,
//         'password': '',
//         'cost': 0,
//         'rank': 1,
//         'img': 'src/app/img/j.jpeg',
//         'user': this.testUsers[0],
//         'courseCategory': this.testCategories[2]
//       },
//       {
//         'id': 2,
//         'name': 'MikroBiologia',
//         'description': 'Budowa mikroorganizmow',
//         'rate': 4,
//         'password': '',
//         'cost': 0,
//         'rank': 2,
//         'img': 'src/app/img/j.jpeg',
//         'user': this.testUsers[0],
//         'courseCategory': this.testCategories[1]
//       },
//       {
//         'id': 3,
//         'name': 'Sieci Neuronowe',
//         'description': 'Uczenie sie maszyn',
//         'rate': 3,
//         'password': '',
//         'cost': 0,
//         'rank': 1,
//         'img': 'src/app/img/j.jpeg',
//         'user': this.testUsers[0],
//         'courseCategory': this.testCategories[0]
//       },
//       {
//         'id': 4,
//         'name': 'Programowanie obiektowe Java',
//         'description': 'Podstawy programowania Javy zmienne',
//         'rate': 3,
//         'password': '',
//         'cost': 0,
//         'rank': 3,
//         'img': 'src/app/img/j.jpeg',
//         'user': this.testUsers[0],
//         'courseCategory': this.testCategories[0]
//       },
//       {
//         'id': 5,
//         'name': 'Kinematyka',
//         'description': 'Zjawisko kinematyku w zyciu codziennym',
//         'rate': 4,
//         'password': '',
//         'cost': 0,
//         'rank': 2,
//         'img': 'src/app/img/j.jpeg',
//         'user': this.testUsers[0],
//         'courseCategory': this.testCategories[3]
//       }];
//     this.testStandardLessonCourse = [
//       {
//         'id': 1,
//         'name': 'Wstep do Programowania',
//         'cost': 0,
//         'course': this.testData[0]
//       },
//       {
//         'id': 2,
//         'name': 'Pierwszy program',
//         'cost': 0,
//         'course': this.testData[0]
//       },
//       {
//         'id': 3,
//         'name': 'Zmienne',
//         'cost': 0,
//         'course': this.testData[0]
//       },
//       {
//         'id': 4,
//         'name': 'Funkcje',
//         'cost': 0,
//         'course': this.testData[0]
//       },
//       {
//         'id': 5,
//         'name': 'Dziedziczenie',
//         'cost': 0,
//         'course': this.testData[0]
//       }
//     ];
//     this.testExtraLessonCourse = [
//       {
//         'id': 1,
//         'name': 'Przepełnienie tablicy a wirusy komputerowe',
//         'cost': 120,
//         'course': this.testData[0]
//       },
//       {
//         'id': 2,
//         'name': 'Wskazniki',
//         'cost': 250,
//         'course': this.testData[0]
//       },
//     ];
//     this.testTestLessonCourse = [
//       {
//         'id': 1,
//         'name': 'Sprawdzian 1',
//         'cost': 250,
//         'course': this.testData[0]
//       },
//       {
//         'id': 2,
//         'name': 'Sprawdzian 2',
//         'cost': 210,
//         'course': this.testData[0]
//       },
//     ];
//     // this.testStandardLessonUser = [
//     //   {
//     //     'id': 1,
//     //     'lessonCourse': this.testStandardLessonCourse[0],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': true,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 2,
//     //     'lessonCourse': this.testStandardLessonCourse[1],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 3,
//     //     'lessonCourse': this.testStandardLessonCourse[2],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 4,
//     //     'lessonCourse': this.testStandardLessonCourse[3],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 5,
//     //     'lessonCourse': this.testStandardLessonCourse[4],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     // ];
//     // this.testExtraLessonUser = [
//     //   {
//     //     'id': 1,
//     //     'lessonCourse': this.testExtraLessonCourse[0],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 2,
//     //     'lessonCourse': this.testExtraLessonCourse[1],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     // ];
//     // this.testTestLessonUser = [
//     //   {
//     //     'id': 6,
//     //     'lessonCourse': this.testTestLessonCourse[0],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     //   {
//     //     'id': 7,
//     //     'lessonCourse': this.testTestLessonCourse[1],
//     //     'user': this.testUsers[0],
//     //     'completed': false,
//     //     'unlock': false,
//     //     'result': 0
//     //   },
//     // ];
//     this.testElementLesson = [
//       {
//         'id': 1,
//         'name': 'Wprowadzenie',
//         'header': 'Wprowadzenie',
//         'content': 'Jezyk java jest jezykiem obiektowym',
//         'type': 0
//       },
//       {
//         'id': 2,
//         'name': 'Podstawowe Algorytmy',
//         'header': 'Podstawowe Algorytmy',
//         'content': 'Sortowanie babelkowe <br> Sortowanie przez wstawianie <br> Sortowanie przez scalanie',
//         'type': 0
//       },
//       {
//         'id': 3,
//         'name': 'Pytania',
//         'header': 'Test wprowadzajacy',
//         'content': '',
//         'type': 1
//       }
//     ];
//     this.testLessonCourseElement = [
//       {
//         'id': 1,
//         'lessonCourse': this.testStandardLessonCourse[0],
//         'elementLesson': this.testElementLesson[0]
//       },
//       {
//         'id': 2,
//         'lessonCourse': this.testStandardLessonCourse[0],
//         'elementLesson': this.testElementLesson[1]
//       },
//       {
//         'id': 3,
//         'lessonCourse': this.testStandardLessonCourse[0],
//         'elementLesson': this.testElementLesson[2]
//       }
//     ];
//     this.testQuestionContent = [
//       {
//         'id': 1,
//         'content': 'Jezyk java jest:'
//       },
//       {
//         'id': 2,
//         'content': 'Typ całkowitoliczbowy to:'
//       },
//     ];
//     this.testQuestionVariants = [
//       {
//         'id': 1,
//         'variant': this.testVariant[0],
//         'questionContent': this.testQuestionContent[0]
//       },
//       {
//         'id': 2,
//         'variant': this.testVariant[1],
//         'questionContent': this.testQuestionContent[0]
//       },
//       {
//         'id': 3,
//         'variant': this.testVariant[2],
//         'questionContent': this.testQuestionContent[0]
//       },
//       {
//         'id': 4,
//         'variant': this.testVariant[3],
//         'questionContent': this.testQuestionContent[0]
//       },
//       {
//         'id': 5,
//         'variant': this.testVariant[4],
//         'questionContent': this.testQuestionContent[1]
//       },
//       {
//         'id': 6,
//         'variant': this.testVariant[5],
//         'questionContent': this.testQuestionContent[1]
//       },
//       {
//         'id': 7,
//         'variant': this.testVariant[6],
//         'questionContent': this.testQuestionContent[1]
//       },
//       {
//         'id': 8,
//         'variant': this.testVariant[7],
//         'questionContent': this.testQuestionContent[1]
//       },
//     ];
//     this.testVariant = [
//       {
//         id: 1,
//         content: 'jezykiem obiektowym',
//         good: 1
//       },
//       {
//         id: 2,
//         content: 'jezykiem strukturalnym',
//         good: 0
//       },
//       {
//         id: 2,
//         content: 'jezykiem zdarzeniowym',
//         good: 0
//       },
//       {
//         id: 3,
//         content: 'jezykiem liniowym',
//         good: 0
//       },
//       {
//         id: 4,
//         content: 'String',
//         good: 0
//       },
//       {
//         id: 5,
//         content: 'int',
//         good: 0
//       },
//       {
//         id: 6,
//         content: 'float',
//         good: 0
//       },
//       {
//         id: 7,
//         content: 'double',
//         good: 0
//       },
//     ];
//     this.testQuestion = [
//       {
//         'id': 1,
//         'elementLesson': this.testElementLesson[2],
//         'questionContent': this.testQuestionContent[0]
//       },
//       {
//         'id': 2,
//         'elementLesson': this.testElementLesson[2],
//         'questionContent': this.testQuestionContent[1]
//       }
//     ];
//   }
// }
//
// // export interface Question {
// // id: number;
// // elementLesson: ElementLesson;
// // questionContent: QuestionContent;
// // }
