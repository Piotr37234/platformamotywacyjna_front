import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdytorKursowComponent } from './edytor-kursow.component';

describe('EdytorKursowComponent', () => {
  let component: EdytorKursowComponent;
  let fixture: ComponentFixture<EdytorKursowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdytorKursowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdytorKursowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
