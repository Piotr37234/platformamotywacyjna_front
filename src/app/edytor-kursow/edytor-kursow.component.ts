import {Component, OnInit} from '@angular/core';
import {ElementLessonPacket, LessonCoursePacket, LessonUserPacket} from '../dataPacket';
import {CourseToolsService} from '../course-tools.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {EditorToolsService} from '../editor-tools.service';

@Component({
  selector: 'app-edytor-kursow',
  templateUrl: './edytor-kursow.component.html',
  styleUrls: ['./edytor-kursow.component.css']
})
export class EdytorKursowComponent implements OnInit {
  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private router: Router, private edytor: EditorToolsService) {
  }

  ngOnInit() {
 console.log(this.edytor.getEditedCourse());
 // this.edytor.getEditedCourse().password = 'dziala';
 // this.edytor.updateSelectedCourse().subscribe();
 // console.log(this.edytor.getPreviousLessonIndex(2));

    // console.log(this.edytor.getEditedCourse().lessonCoursePackets[1]);
    // console.log(this.getLessonUserLoggedUser(this.getStandardLessonSelectedCourse()[1]));
    // console.log('standard');
    // console.log(this.getStandardLessonSelectedCourse());
    // console.log(this.getLessonUserLoggedUser(this.getStandardLessonSelectedCourse()[0]));
  }

  getStandardLessonEditedCourse(): LessonCoursePacket[] {
    return this.edytor.getArrayStandardLessonEditedCourse();
  }

  getExtraLessonEditedCourse(): LessonCoursePacket[] {
    return this.edytor.getArrayExtraLessonEditedCourse();
  }

  getTestLessonEditedCourse():  LessonCoursePacket[] {
    return this.edytor.getArrayTestLessonEditedCourse();
  }

  getDataLoggedUserAboutLesson(lessonCoursePacket: LessonCoursePacket): LessonUserPacket {
    return this.courseToolsService.getLessonUserLoggedUser(lessonCoursePacket);
  }

  setEditedLessonCourse(LessonCourse: any) {
    this.courseToolsService.setSelectedLessonCourse(LessonCourse);
    this.router.navigate(['/lekcja-kurs']);
  }

  editLesson(lesson: LessonCoursePacket) {
    this.edytor.setEditedLesson(lesson);
    this.router.navigate(['edytor-lekcja']);
  }

  newLesson() {
    this.edytor.loadNewLesson();
    this.router.navigate(['edytor-lekcja']);

  }

  save() {
    this.edytor.updateSelectedCourse().subscribe();
    this.router.navigate(['konto']);
    console.log(this.edytor.getEditedCourse());
  }


  deleteLesson(id: number) {
    this.edytor.deleteLesson(id);
    this.edytor.updateSelectedCourse().subscribe();
  }

  left(id1: number, id2: number) {
    this.edytor.swapLesson(id1, id2);

    console.log(this.edytor.getEditedCourse());
  }

  right(id1: number, id2: number) {
    this.edytor.swapLesson(id2, id1);
  }

  getUnlockIf(lesson: LessonCoursePacket) {
    return this.edytor.getStringIfToUnlock(lesson);
  }

  getCategory(lesson: LessonCoursePacket) {
    return this.edytor.getStringCategoryLesson(lesson);
  }

}

