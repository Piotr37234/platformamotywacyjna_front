import { Component, OnInit } from '@angular/core';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {UserPacket} from '../dataPacket';

@Component({
  selector: 'app-strona-glowna',
  templateUrl: './strona-glowna.component.html',
  styleUrls: ['./strona-glowna.component.css']
})
export class StronaGlownaComponent implements OnInit {

  constructor(private user: UserLoggedToolsService) { }
  registry: boolean;
  ngOnInit() {
    this.registry = false;
  }
 getUserLogged(): UserPacket {
    return this.user.getUserLogged();
 }
 clickRegistry() {
   this.registry = true;
 }
 clickLogin() {
   this.registry = false;
 }
}
