import { TestBed, inject } from '@angular/core/testing';

import { UserLoggedToolsService } from './user-logged-tools.service';

describe('UserLoggedToolsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserLoggedToolsService]
    });
  });

  it('should be created', inject([UserLoggedToolsService], (service: UserLoggedToolsService) => {
    expect(service).toBeTruthy();
  }));
});
