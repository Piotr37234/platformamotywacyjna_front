import {Component, OnInit} from '@angular/core';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {UserPacket} from '../dataPacket';

@Component({
  selector: 'app-panel-rejestracji',
  templateUrl: './panel-rejestracji.component.html',
  styleUrls: ['./panel-rejestracji.component.css']
})
export class PanelRejestracjiComponent implements OnInit {
  plec: string;

  constructor(private user: UserLoggedToolsService, private router: Router) {
  }

  username: string;
  password: string;
  rep_password: string;
  errorExist: boolean;
  errorNoTheSame: boolean;
  errorNoDate: boolean;
  noError: boolean;

  ngOnInit() {
    this.username = '';
    this.password = '';
    this.rep_password = '';
    this.plec = 'Mężczyzna';
    this.errorExist = false;
    this.errorNoTheSame = false;
    this.errorNoDate = false;
    this.noError = false;
  }

  zarejestrujKlik(e) {
    this.user.getAllUsers().subscribe(value => {
      const userPacket: UserPacket = new class implements UserPacket {
        completedCourse: number;
        correctAnswer: number;
        createCurses: number;
        effectiveness: number;
        errorAnswer: number;
        experience: number;
        id: number;
        img: string;
        money: number;
        password: string;
        username: string;
      };
      this.testCorrect(value);

      if (this.noError) {
        console.log('noError' + this.noError);
        console.log('bezbledne');

        userPacket.username = this.username;
        userPacket.password = this.password;
        userPacket.completedCourse = 0;
        userPacket.correctAnswer = 0;
        userPacket.createCurses = 0;
        userPacket.effectiveness = 0;
        userPacket.errorAnswer = 0;
        userPacket.experience = 0;

        if (this.plec === 'Mężczyzna') {
          userPacket.img = 'src/app/img/profil.png';
        } else {
          userPacket.img = 'src/app/img/profil2.png';
        }
        userPacket.money = 0;
        this.user.createUser(userPacket).subscribe();
        this.username = '';
        this.password = '';
        this.rep_password = '';
        this.router.navigate(['']);
      }
    });
  }

  wyborPlci(e) {
    const data = e.target.text;
    this.plec = data;
  }


  testErrorExist(users: UserPacket[]) {
    users.forEach(value1 => {
        if (value1.username === this.username) {
          console.log(value1.username);
          console.log(this.username);
          this.errorExist = true;
        }
      }
    );
    this.errorExist = false;
  }


  testErrorNoTheSame() {
    if (this.password === this.rep_password) {
      this.errorNoTheSame = false;
    } else {
      this.errorNoTheSame = true;
    }
  }

  testErrorNoDate() {
    if (this.username !== '' && this.password !== '' && this.rep_password !== '') {
      this.errorNoDate = false;
    } else {
      this.errorNoDate = true;
    }
  }

  testNoError() {
    if (!this.errorNoDate && !this.errorExist && !this.errorNoTheSame) {
      this.noError = true;
    }
  }

  testCorrect(value) {
    this.testErrorNoDate();
    if (!this.errorNoDate) {
      this.testErrorExist(value);
      this.testErrorNoTheSame();
    }
    this.testNoError();
    console.log('errorNoTheSame' + this.errorNoTheSame);
    console.log('errorNoDate' + this.errorNoDate);
    console.log('errorExist' + this.errorExist);
  }



}
