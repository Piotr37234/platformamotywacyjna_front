import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelRejestracjiComponent } from './panel-rejestracji.component';

describe('PanelRejestracjiComponent', () => {
  let component: PanelRejestracjiComponent;
  let fixture: ComponentFixture<PanelRejestracjiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelRejestracjiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelRejestracjiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
