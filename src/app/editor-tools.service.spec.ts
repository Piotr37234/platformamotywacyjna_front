import { TestBed, inject } from '@angular/core/testing';

import { EditorToolsService } from './editor-tools.service';

describe('EditorToolsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditorToolsService]
    });
  });

  it('should be created', inject([EditorToolsService], (service: EditorToolsService) => {
    expect(service).toBeTruthy();
  }));
});
