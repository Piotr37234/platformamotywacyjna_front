import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from './data.service';
import {Observable} from 'rxjs';
import {UserLoggedToolsService} from './user-logged-tools.service';
import {getProjectAsAttrValue} from '@angular/core/src/render3/node_selector_matcher';

import {
  AnswerPacket, CategoryPacket,
  CoursePacket,
  ElementLessonPacket,
  LessonCoursePacket,
  LessonUserPacket, ModifyLessonUserPacket,
  OdpPacket,
  QuestionPacket, ResultElementLesson, UserPacket
} from './dataPacket';

@Injectable({
  providedIn: 'root'
})
export class CourseToolsService {
  private selectCoursePacket: CoursePacket;
  private selectLessonCoursePacket: LessonCoursePacket;

  constructor(private dataService: DataService, private user: UserLoggedToolsService) {
    this.selectCoursePacket = null;
    this.selectLessonCoursePacket = null;
  }

  setSelectedLessonCourse(lessonCoursePacket: LessonCoursePacket) {
    this.selectLessonCoursePacket = lessonCoursePacket;
  }

  getSelectedLessonCourse(): LessonCoursePacket {
    return this.selectLessonCoursePacket;
  }

  setSelectedCourse(coursePacket: CoursePacket) {
    this.selectCoursePacket = coursePacket;
  }

  getSelectCourse(): CoursePacket {
    return this.selectCoursePacket;
  }

  unsetSelectedCourse() {
    this.selectCoursePacket = null;
  }

  unsetSelectedLessonCourse() {
    this.selectLessonCoursePacket = null;
  }

  getAllCourses(): Observable<CoursePacket[]> {
    return this.dataService.getAllCourses();
  }

  getAllCategory(): Observable<CategoryPacket[]> {
    return this.dataService.getAllCategory();
  }

  getCourse(id: number) {
    return this.dataService.getCourse(id);
  }

  saveUserLoggedToCourses(coursePacket: CoursePacket) {
    this.setSelectedCourse(coursePacket);
    return this.dataService.saveUserToCourses(this.user.getUserLogged().id.toString(), coursePacket.id.toString());
  }

  getArrayStandardLessonSelectedCourse(): LessonCoursePacket[] {
    return this.selectCoursePacket.lessonCoursePackets.filter(value => value.type === 1);
  }

  getArrayStandardLessonCourse(coursePacket: CoursePacket): LessonCoursePacket[] {
    return coursePacket.lessonCoursePackets.filter(value => value.type === 1);
  }

  getLessonCourseLoggedCourse(id: number): LessonCoursePacket {
    return this.getSelectCourse().lessonCoursePackets[id];
  }

  getArrayLessonCourseLoggedCourse(): LessonCoursePacket[] {
    return this.getSelectCourse().lessonCoursePackets;
  }

  getArrayExtraLessonSelectedCourse(): LessonCoursePacket[] {
    return this.selectCoursePacket.lessonCoursePackets.filter(value => value.type === 2);
  }


  getArrayTestLessonSelectedCourse(): LessonCoursePacket[] {
    return this.selectCoursePacket.lessonCoursePackets.filter(value => value.type === 3);
  }

  getLessonUserLoggedUser(lessonCoursePacket: LessonCoursePacket): LessonUserPacket {
    // console.log(lessonCoursePacket.lessonUsers.filter(value => value.username === this.user.getUserLogged().username)[0]);
    return lessonCoursePacket.lessonUsers.filter(value => value.username === this.user.getUserLogged().username)[0];
  }

  getLessonUserLoggedUserOpenLesson(): LessonUserPacket {
    return this.getSelectedLessonCourse().lessonUsers.find(value => value.username === this.user.getUserLogged().username);
  }


  getDataLoggedUserAboutLessonSelectLessonCourse(): LessonUserPacket {
    return this.selectLessonCoursePacket.lessonUsers.filter(value => value.username === this.user.getUserLogged().username)[0];
  }



  getArrayElementSelectedCourseLesson(): ElementLessonPacket[] {
    return this.selectLessonCoursePacket.elementLessons;
  }

  getElementSelectedCourseLesson(id: number): ElementLessonPacket {
    return this.selectLessonCoursePacket.elementLessons[id];
  }

  getArrayQuestionsElementLesson(elementLesson: ElementLessonPacket): QuestionPacket[] {
    return elementLesson.questionPackets;
  }

  getArrayAnswerQuestion(question: QuestionPacket): AnswerPacket[] {
    return question.answerPackets;
  }

  isElementQuestion(elementLessonPacket: ElementLessonPacket): boolean {
    if (elementLessonPacket.type === 1) {
      return false;
    } else {
      return true;
    }
  }

  getStringTypeSelectedLessonCourse(): string {
    if (this.getSelectedLessonCourse().type === 1) {
      return 'lekcja';
    } else if (this.getSelectedLessonCourse().type === 2) {
      return 'Ciekawostka';
    } else {
      return 'Test';
    }
  }

  checkOdpElement(elementLessonPacket: ElementLessonPacket): ResultElementLesson {
    const resultElementLesson: ResultElementLesson = new class implements ResultElementLesson {
      completed: boolean;
      correctQuestion: number;
      errorQuestion: number;
      pkt: number;
    };
    resultElementLesson.pkt = 0;
    resultElementLesson.correctQuestion = 0;
    resultElementLesson.errorQuestion = 0;
    resultElementLesson.completed = false;
    let errorAnswer: AnswerPacket[];
    // let odp: AnswerPacket[];
    elementLessonPacket.questionPackets.forEach(questionPacket => {
      const pktForOne = questionPacket.pkt;
      errorAnswer = questionPacket.answerPackets.filter(answer => answer.good === false && answer.odp === true);
      // odp = questionPacket.answerPackets.filter(value => value.odp === true);
      // const countOdp = odpGoodAnswer.length;
      if (errorAnswer.length === 0) {
        resultElementLesson.pkt = resultElementLesson.pkt + pktForOne;
        resultElementLesson.correctQuestion++;
      } else {
        resultElementLesson.errorQuestion++;
      }
    });
    if (resultElementLesson.errorQuestion === 0) {
      resultElementLesson.completed = true;
    }
    return resultElementLesson;
  }

  checkOdpLessonCourse(lessonCoursePacket: LessonCoursePacket) {
    const resultLesson: ResultElementLesson = new class implements ResultElementLesson {
      completed: boolean;
      correctQuestion: number;
      errorQuestion: number;
      pkt: number;
    };
    resultLesson.completed = false;
    resultLesson.correctQuestion = 0;
    resultLesson.errorQuestion = 0;
    resultLesson.pkt = 0;
    lessonCoursePacket.elementLessons.forEach(elementLesson => {
      const result = this.checkOdpElement(elementLesson);
        resultLesson.pkt = resultLesson.pkt + result.pkt;
        resultLesson.correctQuestion = resultLesson.correctQuestion + result.correctQuestion;
        resultLesson.errorQuestion = resultLesson.errorQuestion + result.errorQuestion;

      console.log(result);
      console.log(resultLesson);
    });
    if (resultLesson.errorQuestion === 0) {
      resultLesson.completed = true;
    } else {
      resultLesson.completed = false;
    }
    return resultLesson;
  }

  getMaxPktInLessonCourse(lessonCourse: LessonCoursePacket): number {
    let pkt = 0;
    lessonCourse.elementLessons.forEach(element => {
      element.questionPackets.forEach(question => {
        pkt = pkt + question.pkt;
      });
    });
    return pkt;
  }

  getPktMaxLoggedUserForLessonCourse(lessonCourse: LessonCoursePacket): number {
    const max = this.getMaxPktInLessonCourse(lessonCourse);
    const trials = this.getLessonUserLoggedUser(lessonCourse).trials;
    return Math.round(max / (1 + trials));
  }

  getPktMaxLoggedUserForSelectedLessonCourse(): number {
    const max = this.getMaxPktInLessonCourse(this.getSelectedLessonCourse());
    const trials = this.getDataLoggedUserAboutLessonSelectLessonCourse().trials;
    return Math.round(max / (1 + trials));
  }

  getCoursesCreateUserLogged(coursePacket: CoursePacket[]): CoursePacket[] {
    return coursePacket.filter(value => value.autor.id === this.user.getUserLogged().id);
  }

  getArrayLessonUsersCourse(coursePacket: CoursePacket[]): LessonUserPacket[] {
    let lessonUserPacket: LessonUserPacket[] = null;
    let i = 0;
    coursePacket.forEach(course => {
      course.lessonCoursePackets.forEach(lessonCourse => {
        lessonCourse.lessonUsers.forEach(lessonUser => {
          lessonUserPacket[i] = lessonUser;
          i++;
        });
      });
    });
    return lessonUserPacket;
  }

  getCoursesSaveUserLogged(coursePacket: CoursePacket[]): CoursePacket[] {
    let courseUserSaveLoggedPacket: CoursePacket[] = null;
    let i = 0;
    coursePacket.forEach(course => {
      course.lessonCoursePackets.forEach(lessonCourse => {
        if (lessonCourse.lessonUsers[0].username === this.user.getUserLogged().username) {
          courseUserSaveLoggedPacket[i] = course;
          i++;
        }
      });
    });
    return courseUserSaveLoggedPacket;
  }

  isUserLoggedSavedCourse(coursePacket: CoursePacket) {

    if (typeof coursePacket.lessonCoursePackets[0] === 'undefined') {
      return false;
    } else if (typeof coursePacket.lessonCoursePackets[0].lessonUsers[0] === 'undefined') {
      return false;
    } else if (coursePacket.lessonCoursePackets[0].lessonUsers[0].username === this.user.getUserLogged().username) {
      return true;
    } else {
      return false;
    }
  }

  isUserLoggedCreateCourses(coursePacket: CoursePacket) {
    if (coursePacket.autor.id === this.user.getUserLogged().id) {
      return true;
    }
    else {
      return false;
    }
  }

  modifyLessoUserSelectedCourse(modifyLessonUserPacket: ModifyLessonUserPacket) {
    console.log(this.getArrayLessonCourseLoggedCourse());
    console.log(modifyLessonUserPacket.idLesson);
    let id = modifyLessonUserPacket.idLesson - 1;
    this.getLessonUserLoggedUser(this.getLessonCourseLoggedCourse(id)).result = modifyLessonUserPacket.result;
    this.getLessonUserLoggedUser(this.getLessonCourseLoggedCourse(id)).correctQuestion = modifyLessonUserPacket.correctQuestion;
    this.getLessonUserLoggedUser(this.getLessonCourseLoggedCourse(id)).unlocked = modifyLessonUserPacket.unlock;
    this.getLessonUserLoggedUser(this.getLessonCourseLoggedCourse(id)).errorQuestion = modifyLessonUserPacket.errorQuestion;
    this.getLessonUserLoggedUser(this.getLessonCourseLoggedCourse(id)).trials = modifyLessonUserPacket.trials;
  }

  modifyLessonUser(idLesson: number, result: number, correctQuestion: number, errorQuestion: number, trials: number, unlock: number, completed: number) {
    const modifyLessonUserPacket: ModifyLessonUserPacket = new class implements ModifyLessonUserPacket {
      completed: number;
      correctQuestion: number;
      errorQuestion: number;
      idLesson: number;
      idUser: number;
      result: number;
      trials: number;
      unlock: number;
    };
    modifyLessonUserPacket.result = result;
    modifyLessonUserPacket.correctQuestion = correctQuestion;
    modifyLessonUserPacket.errorQuestion = errorQuestion;
    modifyLessonUserPacket.trials = trials;
    modifyLessonUserPacket.unlock = unlock;
    modifyLessonUserPacket.completed = completed;
    modifyLessonUserPacket.idUser = this.user.getUserLogged().id;
    modifyLessonUserPacket.idLesson = idLesson;
    this.modifyLessoUserSelectedCourse(modifyLessonUserPacket);

    return this.dataService.modifyLessonUser(modifyLessonUserPacket);
  }


  updateCourse(course: CoursePacket) {
    const coursePack: CoursePacket = new class implements CoursePacket {
      autor: UserPacket;
      category: CategoryPacket;
      cost: number;
      description: string;
      id: number;
      img: string;
      lessonCoursePackets: LessonCoursePacket[];
      name: string;
      password: string;
      rank: number;
      rate: number;
    };
    coursePack.autor = course.autor;
    coursePack.category = course.category;
    coursePack.cost = course.cost;
    coursePack.description = course.description;
    coursePack.id = course.id;
    coursePack.img = course.img;
    coursePack.lessonCoursePackets = course.lessonCoursePackets;
    coursePack.name = course.name;
    coursePack.password = course.password;
    coursePack.rank = course.rank;
    coursePack.rate = course.rate;
    return this.dataService.updateCourse(coursePack);
  }

  updateSelectedCourse() {
    return this.dataService.updateCourse(this.getSelectCourse());
  }

  getNextLesson(): LessonCoursePacket {
    const indexTabLesson = this.getSelectCourse().lessonCoursePackets.findIndex(value => value === this.getSelectedLessonCourse());
    return this.getSelectCourse().lessonCoursePackets.find((value, index) => index === indexTabLesson + 1);
  }

  getLessonUserLoggedUserNextLesson(): LessonUserPacket {
    return this.getNextLesson().lessonUsers.find(value => value.username === this.user.getUserLogged().username);
  }


}

//     coursePacket.filter(course => course.lessonCoursePackets.forEach( lessonCourse => {
// lessonCourse.lessonUsers.filte
//     });

