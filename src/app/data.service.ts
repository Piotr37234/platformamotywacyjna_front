import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CategoryPacket, CoursePacket, LessonCoursePacket, ModifyLessonUserPacket, TransferMoneyPacket, UserPacket} from './dataPacket';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) {
  }

  getFile(url: string): Observable<File> {
    return this.http.get<File>(url);
  }

  getAllCourses(): Observable<CoursePacket[]> {
    return this.http.get<CoursePacket[]>('http://localhost:4200/api/course/getAll');
  }

  getAllUsers(): Observable<UserPacket[]> {
    return this.http.get<UserPacket[]>('http://localhost:4200/api/user/showAllUsers');
  }

  getAllCategory(): Observable<CategoryPacket[]> {
    return this.http.get<CategoryPacket[]>('http://localhost:4200/api/course/getAllCategory');
  }



  getCourse(id: number): Observable<CoursePacket> {
    return this.http.get<CoursePacket>('http://localhost:4200/api/course/get/' + id);
  }

  getAccountUser(username: string, password: string): Observable<UserPacket> {
    return this.http.post<UserPacket>('http://localhost:4200/api/user/getAccountUser', {username: username, password: password});
  }

  saveUserToCourses(idUser: string, idCourse: string) {
    return this.http.post('http://localhost:4200/api/course/saveUserToCourse', {idUser: idUser, idCourse: idCourse});
  }

  addMoney(idUser: number, value: number): Observable<TransferMoneyPacket> {
    return this.http.post<TransferMoneyPacket>('http://localhost:4200/api/user/addMoney', {idUser: idUser, value: value});
  }




  modifyLessonUser(modifyLessonUserPacket: ModifyLessonUserPacket): Observable<ModifyLessonUserPacket> {
    return this.http.post<ModifyLessonUserPacket>('http://localhost:4200/api/course/modifyLessonUser', {
      idUser: modifyLessonUserPacket.idUser,
      idLesson: modifyLessonUserPacket.idLesson,
      unlock: modifyLessonUserPacket.unlock,
      completed: modifyLessonUserPacket.completed,
      correctQuestion: modifyLessonUserPacket.correctQuestion,
      errorQuestion: modifyLessonUserPacket.errorQuestion,
      trials: modifyLessonUserPacket.trials,
      result: modifyLessonUserPacket.result
    });
  }

  updateCourse(course: CoursePacket): Observable<CoursePacket> {
    return this.http.post<CoursePacket>('http://localhost:4200/api/course/update', {
      id: course.id,
      name: course.name,
      description: course.description,
      rate: course.rate,
      password: course.password,
      cost: course.cost,
      rank: course.rank,
      img: course.img,
      category: course.category,
      autor: course.autor,
      lessonCoursePackets: course.lessonCoursePackets
    });
  }

  createCourse(course: CoursePacket): Observable<CoursePacket> {
    return this.http.post<CoursePacket>('http://localhost:4200/api/course/create', {
      id: course.id,
      name: course.name,
      description: course.description,
      rate: course.rate,
      password: course.password,
      cost: course.cost,
      rank: course.rank,
      img: course.img,
      category: course.category,
      autor: course.autor,
      lessonCoursePackets: course.lessonCoursePackets
    });
  }


  createUser(user: UserPacket): Observable<UserPacket> {
    return this.http.post<UserPacket>('http://localhost:4200/api/user/create', {
      id: user.id,
      username: user.username,
      password: user.password,
      money: user.money,
      img: user.img,
      completedCourse: user.completedCourse,
      errorAnswer: user.errorAnswer,
      correctAnswer: user.correctAnswer,
      effectiveness: user.effectiveness,
      createCurses: user.createCurses,
      experience: user.experience,
    });
  }


  deleteCourse(id: number) {
    return this.http.delete<CoursePacket>('http://localhost:4200/api/course/delete/' + id);
  }


}

// export interface User {
//   id: number;
//   username: string;
//   password: string;
//   money: number;
// }
// export interface CourseCategory {
//   id: number;
//   name: string;
// }
// export interface Course {
//   id: number;
//   name: string;
//   description: string;
//   rate: number;
//   password: string;
//   cost: number;
//   rank: number;
//   img: string;
//   category: CourseCategory;
//   autor: User;
// }
// export interface Answer {
//   id: number;
//   content: String;
//   good: number;
// }
// export interface Question {
//   id: number;
//   content: String;
//   answers: Answer[];
// }


// var json = '{
// "id": 1,
//   "name": "Java kurs",
//   "description": "To jest kurs javy",
//   "rate": 2,
//   "password": "",
//   "cost": 123,
//   "rank": 2,
//   "img": "string",
//   "category": {
//   "id": 1, "name": "Informatyka"
// },
//   "autor": {
//   "id": 3,
//     "username": "Marek2",
//     "password": "J",
//     "money": 212.0
// },
//   "lessonCoursePackets": [
//   {
//     "id": 1,
//     "name": "Wprowadzenie do Programowania",
//     "cost": 0,
//     "type": 1,
//     "elementLessons": [
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 2,
//         "name": null,
//         "header": null,
//         "content": "Pytanie",
//         "type": 0,
//         "questionPackets": [
//           {
//             "id": 1,
//             "content": "Java to jezyk",
//             "answerPackets": [
//               {
//                 "id": 1,
//                 "content": "obiektowy",
//                 "good": 1
//               },
//               {
//                 "id": 2,
//                 "content": "strukturalny",
//                 "good": 0
//               },
//               {
//                 "id": 3,
//                 "content": "inny",
//                 "good": 0
//               }
//             ]
//           },
//           {
//             "id": 2,
//             "content": "Funkcja wypisujaca tekst na ekranie to",
//             "answerPackets": [
//               {
//                 "id": 4,
//                 "content": "System.out.writeln()",
//                 "good": 0
//               },
//               {
//                 "id": 5,
//                 "content": "System.out.println()",
//                 "good": 1
//               },
//               {
//                 "id": 6,
//                 "content": "System.err.repeat()",
//                 "good": 0
//               }
//             ]
//           }
//         ]
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 2,
//     "name": "Zmienne, Stale",
//     "cost": 250,
//     "type": 1,
//     "elementLessons": [
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 2,
//         "name": null,
//         "header": null,
//         "content": "Pytanie",
//         "type": 0,
//         "questionPackets": [
//           {
//             "id": 1,
//             "content": "Java to jezyk",
//             "answerPackets": [
//               {
//                 "id": 1,
//                 "content": "obiektowy",
//                 "good": 1
//               },
//               {
//                 "id": 2,
//                 "content": "strukturalny",
//                 "good": 0
//               },
//               {
//                 "id": 3,
//                 "content": "inny",
//                 "good": 0
//               }
//             ]
//           },
//           {
//             "id": 2,
//             "content": "Funkcja wypisujaca tekst na ekranie to",
//             "answerPackets": [
//               {
//                 "id": 4,
//                 "content": "System.out.writeln()",
//                 "good": 0
//               },
//               {
//                 "id": 5,
//                 "content": "System.out.println()",
//                 "good": 1
//               },
//               {
//                 "id": 6,
//                 "content": "System.err.repeat()",
//                 "good": 0
//               }
//             ]
//           }
//         ]
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 3,
//     "name": "Programowanie obiektowe",
//     "cost": 245,
//     "type": 1,
//     "elementLessons": [
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 4,
//     "name": "Makrowirusy",
//     "cost": 400,
//     "type": 2,
//     "elementLessons": [
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 5,
//     "name": "Ukryte mozliwosci Javy",
//     "cost": 500,
//     "type": 2,
//     "elementLessons": [
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       },
//       {
//         "id": 1,
//         "name": null,
//         "header": null,
//         "content": "Duzo tresci",
//         "type": 0,
//         "questionPackets": []
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 6,
//     "name": "Test z lekcji 1 i 2",
//     "cost": 0,
//     "type": 3,
//     "elementLessons": [
//       {
//         "id": 2,
//         "name": null,
//         "header": null,
//         "content": "Pytanie",
//         "type": 0,
//         "questionPackets": [
//           {
//             "id": 1,
//             "content": "Java to jezyk",
//             "answerPackets": [
//               {
//                 "id": 1,
//                 "content": "obiektowy",
//                 "good": 1
//               },
//               {
//                 "id": 2,
//                 "content": "strukturalny",
//                 "good": 0
//               },
//               {
//                 "id": 3,
//                 "content": "inny",
//                 "good": 0
//               }
//             ]
//           },
//           {
//             "id": 2,
//             "content": "Funkcja wypisujaca tekst na ekranie to",
//             "answerPackets": [
//               {
//                 "id": 4,
//                 "content": "System.out.writeln()",
//                 "good": 0
//               },
//               {
//                 "id": 5,
//                 "content": "System.out.println()",
//                 "good": 1
//               },
//               {
//                 "id": 6,
//                 "content": "System.err.repeat()",
//                 "good": 0
//               }
//             ]
//           }
//         ]
//       }
//     ],
//     "lessonUsers": []
//   },
//   {
//     "id": 7,
//     "name": "Test z lekcji 3",
//     "cost": 0,
//     "type": 3,
//     "elementLessons": [
//       {
//         "id": 2,
//         "name": null,
//         "header": null,
//         "content": "Pytanie",
//         "type": 0,
//         "questionPackets": [
//           {
//             "id": 1,
//             "content": "Java to jezyk",
//             "answerPackets": [
//               {
//                 "id": 1,
//                 "content": "obiektowy",
//                 "good": 1
//               },
//               {
//                 "id": 2,
//                 "content": "strukturalny",
//                 "good": 0
//               },
//               {
//                 "id": 3,
//                 "content": "inny",
//                 "good": 0
//               }
//             ]
//           },
//           {
//             "id": 2,
//             "content": "Funkcja wypisujaca tekst na ekranie to",
//             "answerPackets": [
//               {
//                 "id": 4,
//                 "content": "System.out.writeln()",
//                 "good": 0
//               },
//               {
//                 "id": 5,
//                 "content": "System.out.println()",
//                 "good": 1
//               },
//               {
//                 "id": 6,
//                 "content": "System.err.repeat()",
//                 "good": 0
//               }
//             ]
//           }
//         ]
//       }
//     ],
//     "lessonUsers": []
//   }
// ]
// }';
//
