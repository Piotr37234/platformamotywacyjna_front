import {Component, OnInit} from '@angular/core';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {UserPacket} from '../dataPacket';
import {Router} from '@angular/router';

@Component({
  selector: 'app-naglowek',
  templateUrl: './naglowek.component.html',
  styleUrls: ['./naglowek.component.css']
})
export class NaglowekComponent implements OnInit {

  constructor(private user: UserLoggedToolsService, private router: Router) {
  }

  ngOnInit() {
  }

  getMoney(): number {
    return this.user.getUserLogged().money;
  }

  getUserLogged(): UserPacket {
    return this.user.getUserLogged();
  }

  isLogged(): boolean {
    if (this.user.getUserLogged() !== null) {
      return true;
    }
    else {
      return false;
    }
  }

  outlogged() {
    this.user.outlogged();
    this.router.navigate(['index']);
    this.ngOnInit();
  }

  // addMoney() {
  //   this.user.addMoney(1, 122).subscribe();
  // }
}
