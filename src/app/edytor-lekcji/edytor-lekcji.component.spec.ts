import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdytorLekcjiComponent } from './edytor-lekcji.component';

describe('EdytorLekcjiComponent', () => {
  let component: EdytorLekcjiComponent;
  let fixture: ComponentFixture<EdytorLekcjiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdytorLekcjiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdytorLekcjiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
