import {Component, OnInit} from '@angular/core';
import {CourseToolsService} from '../course-tools.service';
import {UserLoggedToolsService} from '../user-logged-tools.service';
import {Router} from '@angular/router';
import {
  AnswerPacket, CoursePacket,
  ElementLessonPacket,
  LessonCoursePacket,
  LessonUserPacket,
  QuestionPacket,
  ResultElementLesson,
  ResultLesson
} from '../dataPacket';
import {EditorToolsService} from '../editor-tools.service';
import {max} from 'rxjs/operators';

@Component({
  selector: 'app-edytor-lekcji',
  templateUrl: './edytor-lekcji.component.html',
  styleUrls: ['./edytor-lekcji.component.css']
})
export class EdytorLekcjiComponent implements OnInit {
  active_card_index: number;
  unlockIf: string;
  category: string;
  nameElement: string;
  contentQuestion: string;

  constructor(private courseToolsService: CourseToolsService, private user: UserLoggedToolsService, private router: Router, private editor: EditorToolsService) {
  }

  ngOnInit() {
    this.active_card_index = 0;
    this.category = this.editor.getStringCategoryLesson(this.getEditedLesson());
    this.unlockIf = this.editor.getStringIfToUnlock(this.getEditedLesson());
    // console.log(this.getEditedLesson());
  }

  getData() {
    // return this.data;
  }

  getNumber() {
    return 2;
  }

  getEditedLesson(): LessonCoursePacket {
    return this.editor.getEditedLesson();
  }

  getArrayElementEditedCourseLesson(): ElementLessonPacket[] {
    return this.editor.getArrayElementEditedCourseLesson();
  }

  getActiveCard(): ElementLessonPacket {
    return this.editor.getElementEditedCourseLesson(this.active_card_index);
  }

  clikCard(id: number) {
    this.active_card_index = id;
  }

  setClassActiveCard(id: number): string {
    if (id === this.active_card_index) {
      return 'nav-link active';
    }
    return 'nav-link';
  }

  isBuy(): boolean {
    if (this.unlockIf === 'Poprzednia/Zakup' || this.unlockIf === 'Zakup') {
      return true;
    } else {
      return false;
    }
  }

  refreshIf() {
    if (this.unlockIf === 'Poprzednia/Zakup') {
      this.getEditedLesson().idNext = 2;
      this.getEditedLesson().locked = 1;
    }
    else if (this.unlockIf === 'Zakup') {
      this.getEditedLesson().idNext = 3;
      this.getEditedLesson().locked = 1;
    }
    else if (this.unlockIf === 'Poprzednia') {
      this.getEditedLesson().idNext = 1;
      this.getEditedLesson().locked = 1;
    }
    else {
      this.getEditedLesson().idNext = -1;
      this.getEditedLesson().locked = 0;
    }
  }


  refreshCategory() {
    if (this.category === 'Lekcja') {
      this.getEditedLesson().type = 1;
    }
    else if (this.category === 'Ciekawostka') {
      this.getEditedLesson().type = 2;
    }
    else if (this.category === 'Sprawdzian') {
      this.getEditedLesson().type = 3;
    }
    else {
      this.getEditedLesson().idNext = 0;
    }
  }

  getArrayQuestionsActiveCard(): QuestionPacket[] {
    return this.courseToolsService.getArrayQuestionsElementLesson(this.getActiveCard());
  }

  getArrayAnswerQuestion(question: any): AnswerPacket[] {
    return this.courseToolsService.getArrayAnswerQuestion(question);
  }

  isActiveIsElementQuestion(e): boolean {
    return this.courseToolsService.isElementQuestion(this.getActiveCard());
  }

  checkOdpElement(elementLessonPacket: ElementLessonPacket): ResultElementLesson {
    return this.courseToolsService.checkOdpElement(elementLessonPacket);
  }

  Commit() {
    console.log(this.checkOdpElement(this.getActiveCard()));
  }

  getDataLoggedUserAboutLessonSelectLessonCourse(): LessonUserPacket {
    return this.courseToolsService.getDataLoggedUserAboutLessonSelectLessonCourse();
  }

  // getSelectedLessonCourse(): LessonCoursePacket {
  //   return this.courseToolsService.getSelectedLessonCourse();
  // }

  getStringTypeSelectedLessonCourse(): string {
    return this.courseToolsService.getStringTypeSelectedLessonCourse();
  }

  getStatusColor(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'alert alert-success';
    } else {
      return 'alert alert-warning';
    }
  }

  getPreviousUnlockLesson(): LessonCoursePacket {
    let lessons: LessonCoursePacket[];
    lessons = this.editor.getEditedCourse().lessonCoursePackets.filter(value => value.idNext === this.editor.getEditedLesson().id);
    return lessons[0];
  }

  isIfUnlockPreviousLesson(): boolean {
    let lessons: LessonCoursePacket[];
    lessons = this.editor.getEditedCourse().lessonCoursePackets.filter(value => (value.idNext = this.editor.getEditedLesson().id) && (value.id + 1 === this.editor.getEditedLesson().id));
    if (lessons.length === 1) {
      return true;
    } else {
      return false;
    }
  }

  getPreviousLesson(): LessonCoursePacket {
    let lessons: LessonCoursePacket[];
    lessons = this.editor.getEditedCourse().lessonCoursePackets.filter(value => (value.id + 1 === this.editor.getEditedLesson().id));
    return lessons[0];
  }

  test2() {
    console.log('test2');
    console.log(this.nameElement);
  }

  getStringStatus(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'lekcja ukończona';
    } else {
      return 'w trakcie';
    }
  }

  getButtonStringZatwierdz(): string {
    if (this.getDataLoggedUserAboutLessonSelectLessonCourse().completed) {
      return 'Zakoncz';
    } else {
      return 'Zatwierdz wszystkie pytania i sprawdz';
    }
  }

  getPktMaxLoggedUserForSelectedLessonCourse(): number {
    return this.courseToolsService.getPktMaxLoggedUserForSelectedLessonCourse();
  }

  test() {
    console.log(this.editor.getEditedLesson().name);
  }


  addNewElement() {
    const element: ElementLessonPacket = new class implements ElementLessonPacket {
      content: string;
      header: string;
      id: number;
      name: string;
      questionPackets: QuestionPacket[];
      type: number;
    };

    element.name = this.nameElement;
    element.questionPackets = [];
    element.content = '<h2> Tu wpisz treść </h2>';
    element.type = 1;
    this.editor.addNewElementLessonToEditedLesson(element);
    // console.log(this.editor.getEditedLesson().elementLessons);
  }


  deleteElement(id: number) {
    this.editor.deleteElement(id);
    this.active_card_index = -1;
  }


  addQuestion() {
    const question: QuestionPacket = new class implements QuestionPacket {
      answerPackets: AnswerPacket[];
      content: string;
      id: number;
      pkt: number;
    };
    question.content = this.contentQuestion;
    question.answerPackets = [];
    question.pkt = 10;
    this.editor.addNewQuestion(this.active_card_index, question);
  }


  addAnswer(question: QuestionPacket) {
    const answer: AnswerPacket = new class implements AnswerPacket {
      content: string;
      id: number;
      good: boolean;
      odp: boolean;
    };
    answer.content = 'wariant odpowiedzi';
    this.editor.addAnswer(question, answer);
  }



  deleteQuestion(id: number) {
    this.editor.deleteQuestion(this.getActiveCard(), id);
  }

  deleteAnswer(question: QuestionPacket, id: number) {
    this.editor.deleteAnswer(question, id);
  }

  save() {
    console.log(this.getEditedLesson());
    this.editor.addEditedLessonToEditedCourses();
    this.editor.updateSelectedCourse().subscribe();
    this.router.navigate(['edytor-kurs']);
  }

  isDisplay(): boolean {
    if (this.editor.getEditedLesson().elementLessons.length === 0 || this.active_card_index === -1) {
       return false;
    } else {
       return true;
    }
  }
// saveElementLessonActiveCardToOdpPacket(): OdpPacket {
//
// }
//   getArrayWariantsQuestion(): Wariants[] {
//
//   }
}
