import { TestBed, inject } from '@angular/core/testing';

import { CourseToolsService } from './course-tools.service';

describe('CourseToolsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CourseToolsService]
    });
  });

  it('should be created', inject([CourseToolsService], (service: CourseToolsService) => {
    expect(service).toBeTruthy();
  }));
});
